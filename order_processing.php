<?php

use OrderProcessing\{Order, Processor};
use OrderProcessing\Order\{Delivery, Validator};
use OrderProcessing\Printer\FilePrinter;

define('BASE_PATH', getcwd());

require_once BASE_PATH . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

$fileName = $argv[1];

$orderProcessor = new Processor(
    new Validator(file_get_contents(BASE_PATH . DIRECTORY_SEPARATOR . 'input/minimumAmount')),
    new FilePrinter(BASE_PATH . DIRECTORY_SEPARATOR . 'orderProcessLog'),
    new FilePrinter(BASE_PATH . DIRECTORY_SEPARATOR . 'result'),
);

$fileHandler = fopen($fileName, 'r+');
while ($fields = fgetcsv($fileHandler)) {
    $order = new Order(
        (int)$fields[0],
        $fields[1],
        array_map('intval', explode(',', $fields[2])),
        (float)$fields[3]
    );
    $order->setDelivery(new Delivery($order));
    $orderProcessor->process($order);
}

fclose($fileHandler);
