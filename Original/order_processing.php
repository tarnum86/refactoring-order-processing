<?php

use Orders\Order;
use Orders\OrderDeliveryDetails;
use Orders\OrderProcessor;

require_once dirname(__FILE__).'/../vendor/autoload.php';

$fileName = $argv[1];

$orderProcessor = new OrderProcessor(new OrderDeliveryDetails());

$fileHandler = fopen($fileName, 'r+');
while ($fields = fgetcsv($fileHandler)) {
    $order = new Order();
    $order->setOrderId($fields[0]);
    $order->setName($fields[1]);
    $order->setItems(array_map('intval', explode(',', $fields[2])));
    $order->setTotalAmount($fields[3]);
    $orderProcessor->process($order);
}

fclose($fileHandler);
