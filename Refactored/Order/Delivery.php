<?php

namespace OrderProcessing\Order;

use OrderProcessing\Order;

/**
 * Class Delivery
 * @package OrderProcessing\Order
 */
class Delivery
{
    /**
     * @const array
     */
    public const LARGE_PRODUCTS_IDS = [3231, 9823];
    /**
     * @const int
     */
    public const DAYS_TO_DELIVER_ONE_PRODUCT = 1;
    /**
     * @const int
     */
    public const DAYS_TO_DELIVER_MANY_PRODUCTS = 2;
    /**
     * @var Order
     */
    private Order $order;

    /**
     * Delivery constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function calculateDaysToDeliver(): int
    {
        $productsCount = count($this->order->getItems());
        if ($productsCount > 1) {
            return self::DAYS_TO_DELIVER_MANY_PRODUCTS;
        } else {
            return self::DAYS_TO_DELIVER_ONE_PRODUCT;
        }
    }

    /**
     * @return string
     */
    public function getDetails(): string
    {
        $daysToDeliver = $this->calculateDaysToDeliver();
        return sprintf('Order delivery time: %d day%s', $daysToDeliver, $daysToDeliver > 1 ? 's' : '');
    }

    /**
     * @return float
     */
    public function calculateTotalAmount(): float
    {
        $totalAmount = 0;
        foreach ($this->order->getItems() as $item) {
            if (in_array($item, self::LARGE_PRODUCTS_IDS, true)) {
                $totalAmount += 100;
            }
        }
        return $totalAmount;
    }
}