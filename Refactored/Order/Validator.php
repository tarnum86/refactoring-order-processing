<?php

namespace OrderProcessing\Order;

use OrderProcessing\Order;

/**
 * Class Validator
 * @package OrderProcessing
 */
class Validator
{
    /**
     * @const int
     */
    public const MINIMUM_ORDER_NAME_LENGTH = 3;
    /**
     * @var int
     */
    public int $minimumAmount;

    /**
     * Validator constructor.
     * @param float $minimumAmount
     */
    public function __construct(float $minimumAmount)
    {
        $this->minimumAmount = $minimumAmount;
    }

    /**
     * @param float $minimumAmount
     */
    public function setMinimumAmount(float $minimumAmount)
    {
        $this->minimumAmount = $minimumAmount;
    }

    /**
     * @param Order $order
     * @throws ValidatorException
     */
    public function validate(Order $order): void
    {
        $errors = [];
        if (strlen($order->getName()) < self::MINIMUM_ORDER_NAME_LENGTH) {
            $errors[] = 'Order name must have more than 2 letters.';
        }
        if ($order->getTotalAmount() <= 0) {
            $errors[] = 'Order total amount must be more than 0.';
        }
        if ($order->getTotalAmount() < $this->minimumAmount) {
            $errors[] = 'Order total amount must be not less than minimum.';
        }
        foreach ($order->getItems() as $itemId) {
            if (!is_numeric($itemId)) {
                $errors[] = sprintf('Order items ids must be numeric. Item with id "%s".', $itemId);
            }
        }

        if (!empty($errors)) {
            throw new ValidatorException(implode(' ', $errors));
        }
    }
}