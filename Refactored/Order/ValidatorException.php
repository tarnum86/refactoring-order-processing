<?php

namespace OrderProcessing\Order;

use \Exception;

/**
 * Class ValidatorException
 * @package OrderProcessing\Order
 */
class ValidatorException extends Exception {

}