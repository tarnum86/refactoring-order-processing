<?php

namespace OrderProcessing\Printer;

/**
 * Interface PrinterInterface
 * @package OrderProcessing\Printer
 */
interface PrinterInterface
{
    /**
     * @return void
     */
    public function print(): void;
    /**
     * @param string $message
     * @return void
     */
    public function add(string $message): void;
    /**
     * @return array
     */
    public function getMessages(): array;
}