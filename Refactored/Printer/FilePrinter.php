<?php

namespace OrderProcessing\Printer;

/**
 * Class Log
 * @package OrderProcessing
 */
class FilePrinter extends PrinterAbstract implements PrinterInterface
{
    /**
     * @var string
     */
    private string $filePath;

    /**
     * PrinterAbstract constructor.
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return void
     */
    public function print(): void
    {
        $fileHandler = fopen($this->filePath, 'a');
        foreach ($this->messages as $message) {
            fwrite($fileHandler, $message . PHP_EOL);
        }
        fclose($fileHandler);
        $this->messages = [];
    }
}