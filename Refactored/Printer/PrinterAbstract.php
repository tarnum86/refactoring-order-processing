<?php

namespace OrderProcessing\Printer;

/**
 * Class PrinterAbstract
 * @package OrderProcessing\Printer
 */
abstract class PrinterAbstract implements PrinterInterface
{
    /**
     * @var array
     */
    protected array $messages = [];

    /**
     * @param string $message
     * @return void
     */
    public function add(string $message): void
    {
        $this->messages[] = $message;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @return void
     */
    abstract public function print(): void;
}