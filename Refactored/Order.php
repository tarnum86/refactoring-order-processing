<?php

namespace OrderProcessing;

use OrderProcessing\Order\Delivery;

/**
 * Class Order
 * @package OrderProcessing
 */
class Order
{
    /**
     * @var int
     */
    private int $id;
    /**
     * @var string
     */
    private string $name;
    /**
     * @var array
     */
    private array $items;
    /**
     * @var float
     */
    private float $totalAmount;
    /**
     * @var bool
     */
    private bool $isManualProcessing = false;
    /**
     * @var Delivery
     */
    private Delivery $delivery;

    /**
     * Order constructor.
     * @param int $id
     * @param string $name
     * @param array $items
     * @param float $totalAmount
     */
    public function __construct(int $id, string $name,array $items, float $totalAmount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->items = $items;
        $this->totalAmount = $totalAmount;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param float $totalAmount
     */
    public function setTotalAmount(float $totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    /**
     * @param bool $isManualProcessing
     */
    public function setIsManualProcessing(bool $isManualProcessing)
    {
        $this->isManualProcessing = $isManualProcessing;
    }

    /**
     * @param Delivery $delivery
     */
    public function setDelivery(Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): Delivery
    {
        return $this->delivery;
    }

    /**
     * @return bool
     */
    public function getIsManualProcessing(): bool
    {
        return $this->isManualProcessing;
    }

    /**
     * @return float
     */
    public function calculateFinalAmount(): float
    {
        $finalAmount = $this->totalAmount;
        if (!empty($this->delivery)) {
            $finalAmount += $this->delivery->calculateTotalAmount();
        }
        return $finalAmount;
    }
}