<?php

namespace OrderProcessing;

use OrderProcessing\Order\{Validator, ValidatorException};
use OrderProcessing\Printer\PrinterInterface;

/**
 * Class Processor
 * @package OrderProcessing
 */
class Processor
{
    /**
     * @var Validator
     */
	private Validator $validator;
    /**
     * @var PrinterInterface
     */
	private PrinterInterface $logPrinter;
    /**
     * @var PrinterInterface
     */
	private PrinterInterface $resultPrinter;

    /**
     * Processor constructor.
     * @param Validator $validator
     * @param PrinterInterface $logPrinter
     * @param PrinterInterface $resultPrinter
     */
	public function __construct(Validator $validator, PrinterInterface $logPrinter, PrinterInterface $resultPrinter)
	{
		$this->validator = $validator;
		$this->logPrinter = $logPrinter;
		$this->resultPrinter = $resultPrinter;
	}

    /**
     * @param Order $order
     */
	public function process(Order $order)
	{
		$this->logPrinter->add(sprintf('Processing started, OrderId: %d', $order->getId()));
		try {
            $this->validator->validate($order);
            $this->logPrinter->add('Order is valid');
            if ($order->getIsManualProcessing()) {
                $this->logPrinter->add(sprintf('Order "%d" NEEDS MANUAL PROCESSING', $order->getId()));
            } else {
                $this->logPrinter->add(sprintf('Order "%d" WILL BE PROCESSED AUTOMATICALLY', $order->getId()));
            }
            $orderFinalAmount = $order->calculateFinalAmount();
            $this->resultPrinter->add($order->getId() . '-'
                . implode(',', $order->getItems()) . '-'
                . $order->getDelivery()->getDetails() . '-'
                . ($order->getIsManualProcessing() ? 1 : 0) . '-'
                . $orderFinalAmount . '-' . $order->getName());
            $this->resultPrinter->print();
        } catch (ValidatorException $exception) {
            $this->logPrinter->add('Order is invalid');
        }

		$this->logPrinter->print();
	}
}