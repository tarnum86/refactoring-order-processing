<?php

namespace OrderProcessing\Test\Order;

use OrderProcessing\Order;
use OrderProcessing\Order\ValidatorException;
use PHPUnit\Framework\TestCase;
use OrderProcessing\Order\Validator;

/**
 * Class ValidatorTest
 * @package OrderProcessing\Test\Order
 */
class ValidatorTest extends TestCase
{
    /**
     * @covers \OrderProcessing\Order\Validator::validate
     */
    public function testValidateOrderIsValidNotThrowException()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('zxc'));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(123));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['33']));

        $validator = new Validator(1);
        $this->assertEmpty($validator->validate($orderMock));
    }

    /**
     * @covers \OrderProcessing\Order\Validator::validate
     */
    public function testValidateThrowsExceptionWhenSmallName()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('zx'));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(123));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['33']));

        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Order name must have more than 2 letters.');
        $validator = new Validator(1);
        $validator->validate($orderMock);
    }

    /**
     * @covers \OrderProcessing\Order\Validator::validate
     */
    public function testValidateThrowsExceptionWhenTotalOrderAmountIsZero()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('zxr'));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(0));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['33']));

        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage(
            'Order total amount must be more than 0. Order total amount must be not less than minimum.'
        );
        $validator = new Validator(1);
        $validator->validate($orderMock);
    }

    /**
     * @covers \OrderProcessing\Order\Validator::validate
     */
    public function testValidateThrowsExceptionWhenTotalOrderAmountIsLessThanMinimum()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('zxr'));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(10));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['33']));

        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage(
            'Order total amount must be not less than minimum.'
        );
        $validator = new Validator(11);
        $validator->validate($orderMock);
    }

    /**
     * @covers \OrderProcessing\Order\Validator::validate
     */
    public function testValidateThrowsExceptionWhenOrderItemIdIsNotNumeric()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('zxx'));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(123));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['asd']));

        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage(
            'Order items ids must be numeric. Item with id "asd".'
        );
        $validator = new Validator(11);
        $validator->validate($orderMock);
    }
}