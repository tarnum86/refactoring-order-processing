<?php

namespace OrderProcessing\Test\Order;

use OrderProcessing\Order\Delivery;
use OrderProcessing\Order;
use PHPUnit\Framework\TestCase;

/**
 * Class DeliveryTest
 * @package OrderProcessing\Test\Order
 */
class DeliveryTest extends TestCase
{
    /**
     * @covers \OrderProcessing\Order\Delivery::calculateDaysToDeliver
     */
    public function testCalculateDaysToDeliverWhenOrderHasOneItem()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['12']));

        $delivery = new Delivery($orderMock);
        $this->assertEquals(1, $delivery->calculateDaysToDeliver());
    }

    /**
     * @covers \OrderProcessing\Order\Delivery::calculateDaysToDeliver
     */
    public function testCalculateDaysToDeliverWhenOrderHasManyItems()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['12','345']));

        $delivery = new Delivery($orderMock);
        $this->assertEquals(2, $delivery->calculateDaysToDeliver());
    }

    /**
     * @covers \OrderProcessing\Order\Delivery::getDetails
     */
    public function testGetDetailsWhenOrderHasOneItem()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['12']));

        $delivery = new Delivery($orderMock);
        $this->assertEquals('Order delivery time: 1 day', $delivery->getDetails());
    }

    /**
     * @covers \OrderProcessing\Order\Delivery::getDetails
     */
    public function testGetDetailsWhenOrderHasManyItems()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue(['12','345']));

        $delivery = new Delivery($orderMock);
        $this->assertEquals('Order delivery time: 2 days', $delivery->getDetails());
    }

    /**
     * @covers \OrderProcessing\Order\Delivery::calculateTotalAmount
     */
    public function testCalculateTotalAmountWhenOrderHasBigItems()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue([3231, 9823]));

        $delivery = new Delivery($orderMock);
        $this->assertEquals(200, $delivery->calculateTotalAmount());
    }

    /**
     * @covers \OrderProcessing\Order\Delivery::calculateTotalAmount
     */
    public function testCalculateTotalAmountWhenOrderHasNoBigItems()
    {
        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue([123]));

        $delivery = new Delivery($orderMock);
        $this->assertEquals(0, $delivery->calculateTotalAmount());
    }
}