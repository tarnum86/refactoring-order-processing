<?php

namespace OrderProcessing\Test;

use OrderProcessing\Order;
use OrderProcessing\Order\Delivery;
use OrderProcessing\Printer\FilePrinter;
use PHPUnit\Framework\TestCase;
use OrderProcessing\Order\Validator;
use OrderProcessing\Processor;

/**
 * Class ProcessorTest
 * @package OrderProcessing\Test
 */
class ProcessorTest extends TestCase
{
    /**
     * @covers \OrderProcessing\Processor::process
     */
    public function testProcessOrderSingleItem()
    {
        $validator = $this->createMock(Validator::class);

        $logPrinter = $this->getMockBuilder(FilePrinter::class)->disableOriginalConstructor()
            ->setMethodsExcept(['add', 'getMessages'])->getMock();

        $resultPrinter = $this->getMockBuilder(FilePrinter::class)->disableOriginalConstructor()
            ->setMethodsExcept(['add', 'getMessages'])->getMock();

        $deliveryMock = $this->createMock(Delivery::class);
        $deliveryMock->expects($this->any())->method('getDetails')
            ->will($this->returnValue('Order delivery time: 1 day'));

        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getId')->will($this->returnValue(1));
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('Martin Fowler'));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue([6654]));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(346.2));
        $orderMock->expects($this->any())->method('calculateFinalAmount')->will($this->returnValue(346.2));
        $orderMock->expects($this->any())->method('getDelivery')->will($this->returnValue($deliveryMock));

        $orderProcessor = new Processor($validator, $logPrinter, $resultPrinter);
        $orderProcessor->process($orderMock);
        $this->assertEquals([
            'Processing started, OrderId: 1',
            'Order is valid',
            'Order "1" WILL BE PROCESSED AUTOMATICALLY',
        ], $logPrinter->getMessages());
        $this->assertEquals([
            '1-6654-Order delivery time: 1 day-0-346.2-Martin Fowler'
        ], $resultPrinter->getMessages());
    }
    /**
     * @covers \OrderProcessing\Processor::process
     */
    public function testProcessOrderMultipleItems() {
        $validatorMock = $this->createMock(Validator::class);

        $logPrinter = $this->getMockBuilder(FilePrinter::class)->disableOriginalConstructor()
            ->setMethodsExcept(['add', 'getMessages'])->getMock();

        $resultPrinter = $this->getMockBuilder(FilePrinter::class)->disableOriginalConstructor()
            ->setMethodsExcept(['add', 'getMessages'])->getMock();

        $deliveryMock = $this->createMock(Delivery::class);
        $deliveryMock->expects($this->any())->method('getDetails')
            ->will($this->returnValue('Order delivery time: 2 days'));

        $orderMock = $this->createMock(Order::class);
        $orderMock->expects($this->any())->method('getId')->will($this->returnValue(2));
        $orderMock->expects($this->any())->method('getName')->will($this->returnValue('Bob Martin'));
        $orderMock->expects($this->any())->method('getItems')->will($this->returnValue([37, 73]));
        $orderMock->expects($this->any())->method('getTotalAmount')->will($this->returnValue(346.2));
        $orderMock->expects($this->any())->method('calculateFinalAmount')->will($this->returnValue(4242));
        $orderMock->expects($this->any())->method('getDelivery')->will($this->returnValue($deliveryMock));

        $orderProcessor = new Processor($validatorMock, $logPrinter, $resultPrinter);
        $orderProcessor->process($orderMock);
        $this->assertEquals([
            'Processing started, OrderId: 2',
            'Order is valid',
            'Order "2" WILL BE PROCESSED AUTOMATICALLY'
        ], $logPrinter->getMessages());
        $this->assertEquals([
            '2-37,73-Order delivery time: 2 days-0-4242-Bob Martin'
        ], $resultPrinter->getMessages());
    }
}