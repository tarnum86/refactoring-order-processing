<?php

namespace OrderProcessing\Test;

use OrderProcessing\Order;
use OrderProcessing\Order\Delivery;
use PHPUnit\Framework\TestCase;

/**
 * Class OrderTest
 * @package OrderProcessing\Test
 */
class OrderTest extends TestCase
{
    /**
     * @covers \OrderProcessing\Order::calculateFinalAmount
     */
    public function testCalculateFinalAmountWhenDeliveryIsSet()
    {
        $deliveryMock = $this->createMock(Delivery::class);
        $deliveryMock->expects($this->any())->method('calculateTotalAmount')
            ->will($this->returnValue(123.5));
        $order = new Order(3, 'Order Name', ['5','7'], 100.357);
        $order->setDelivery($deliveryMock);
        $this->assertEquals(223.857, $order->calculateFinalAmount());
    }

    /**
     * @covers \OrderProcessing\Order::calculateFinalAmount
     */
    public function testCalculateFinalAmountWhenDeliveryIsNotSet()
    {
        $order = new Order(3, 'Order Name', ['5','7'], 100.357);
        $this->assertEquals(100.357, $order->calculateFinalAmount());
    }
}